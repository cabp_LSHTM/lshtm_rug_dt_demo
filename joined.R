
.args <- c("analyzed.rds", "new_cols.rds", "final.rds")
.args <- commandArgs(trailingOnly = T)

src <- readRDS(.args[1])
ref <- readRDS(.args[2])

# note, this will give use only origins, carriers, dates, etc where there is delay
# greater than 30 mins
joined <- ref[src, on=.(origin, month, day, dep_plan_hour, carrier), nomatch=NA]

res <- joined[,.(median_proportional_delay=median(dep_delay/max)),keyby=.(origin, month, day, dep_plan_hour, carrier)]

saveRDS(res, tail(.args, 1))