
# the first target is what make attempts when invoked
# without a specific target
# "default" doesn't have any special meaning, but it's
# a common way to remind oneself what make will do
default: final.rds

URL := https://raw.githubusercontent.com/wiki/arunsrinivasan/flights/NYCflights14/flights14.csv

# make special variables:
# $< = the first dependency
# $^ = all the dependencies
# $@ = the target

airtravel.csv: io_w_dt.R
	Rscript $< $(URL) $@

basic_filter.rds: basic_filter.R airtravel.csv
	Rscript $^ $@

new_cols.rds: compute_new_cols.R airtravel.csv
	Rscript $^ $@

analyzed.rds: analysis.R new_cols.rds
	Rscript $^ $@

final.rds: joined.R analyzed.rds new_cols.rds
	Rscript $^ $@